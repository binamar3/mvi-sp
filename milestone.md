# Milestone
## Úvod
V rámci této semestrální práce je cílem, využití genetických algoritmů na návrh struktury neuronových sítích na malých datasetech. 

## Rešerše
V rámci přípravy jsem prošel následující papery, které se zabývají podobnou úlohou. 

1. [Automatically Designing CNN Architectures Using
Genetic Algorithm for Image Classification](https://arxiv.org/pdf/1808.03818v3.pdf) 
2. [Genetic CNN](https://arxiv.org/pdf/1703.01513v1.pdf)
3. [EFFICIENT HYPERPARAMETER OPTIMIZATION IN DEEP
LEARNING USING A VARIABLE LENGTH GENETIC ALGORITHM](https://arxiv.org/pdf/2006.12703v1.pdf)

Tyto papery popisují způsob jak řešit návrhu konvolučních neuronových sítí pomocí GA. Liší se převážně jen ve způsobu zakódování jedince a možnostech nastavení hyperparametrů sítě. 

4. [Genetic Network Architecture Search](https://arxiv.org/pdf/1907.02871v1.pdf)

Popisuje použití sdílených vah mezi potomky a díky tomu zmenšit prohledávaný prostor. Integrovali tak GA a gradient descent a nemuseli tak každého potomka trénovat od začátku. Důležitým úkolem je uzpůsobit algoritmus tak, aby váhy se adaptovali při změně architektury.

## Implementace
Současný stav implementace lze najít v notebooku `GENsNN.ipynb` [link](GENsNN.ipynb). Implementace je zatím nedokončená a v rámci evoluce se mění jen počet neuronů ve vrstvách a počet vrstev. V rámci genetického algoritmu je implementována mutace, jednobodový křížení a ruletová selekce. Jako fitness se využívá přesnost na validačním datasetu.

Algoritmus byl otestovaný na omezeném datasetu MNIST a nízkém počtu generací a velikosti populace z důvodu současného omezení trénování na CPU. 

V současné verzi do následující generace přichází nový jedinci trénovaní od začátku a stálo, by za to vyzkoušet nechat některé jedince přežít do následujících generací s již natrénovanými váhami nebo přidat možnost předat váhy z rodičů do potomků.

- Na následujícím grafu je vidět vývoj fitness v průběhu generací.

![first graph](graph.png)

## Zdroje 

| Název | Popis                     | Technologie | URL |
| ----- |---------------------------| --- | --- |
| MNIST | Dataset rukou psaných číslic | PyTorch | https://pytorch.org/vision/stable/datasets.html#mnist |
| AutoCNN | implementace  1. článku   | Tensorflow | https://github.com/Marius-Juston/AutoCNN |
| GeneticNAS | implementace 4. článku | Pytorch | https://github.com/haihabi/GeneticNAS |     