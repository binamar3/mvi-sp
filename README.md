# MVI-sp

# MVI - Semestrální práce
## Téma: Využití genetického algoritmu na návrh neuronové sítě

Milestone pod odkazem [MILESTONE.md](MILESTONE.md).

Zdrojové kódy jsou v Jupyter Notebooku [GENsNN.ipynb](GENsNN.ipynb).

Report v PDF formátu je v souboru [REPORT.pdf](REPORT.pdf)
